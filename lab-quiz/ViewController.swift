//
//  ViewController.swift
//  lab-quiz
//
//  Created by Gregorius Albert on 06/11/21.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var idCardNumberField: UITextField!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBAction func nameFieldExit(_ sender: Any) {
        nameField.resignFirstResponder()
    }
    
    @IBAction func idCardNumberFieldExit(_ sender: Any) {
        idCardNumberField.resignFirstResponder()
    }
    
    
    @IBAction func usernameFieldExit(_ sender: Any) {
        usernameField.resignFirstResponder()
    }
    
    
    @IBAction func passwordFieldExit(_ sender: Any) {
        passwordField.resignFirstResponder()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datepicker.backgroundColor = .white
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    
    func pushAlert(title:String, message:String) -> Void{
        
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "OK",
            style: .default,
            handler: nil
        ))
        
        present(alert, animated: true, completion: nil)
    }
    
    func validateName() -> Void {
        if !nameField.text!.contains(" "){
            pushAlert(title: "Check Name Field!", message: "Name must be at least 2 word!")
        }
    }
    
    func validateIdCardNumber() -> Void {
        
        var containsLetter:Bool = false
        for i in idCardNumberField.text!{
            if i.isLetter{
                containsLetter = true
            }
        }
        
        if idCardNumberField.text?.count != 16 || containsLetter == true{
            pushAlert(title: "Check ID Card Number Field!", message: "ID Card Number must be 16 digits of number!")
        }
    }
    
    var age:Int = 0
    var DOB:String = ""
    
    func validateDOB() -> Void {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        DOB = df.string(from: datepicker.date)
        
        let ddf1 = DateFormatter()
        ddf1.dateFormat = "dd"
        let d1 = ddf1.string(from: datepicker.date)
        
        let mdf1 = DateFormatter()
        mdf1.dateFormat = "MM"
        let m1 = mdf1.string(from: datepicker.date)
        
        let ydf1 = DateFormatter()
        ydf1.dateFormat = "yyyy"
        let y1 = ydf1.string(from: datepicker.date)
        
        let ddf2 = DateFormatter()
        ddf2.dateFormat = "dd"
        let d2 = ddf2.string(from: Date())
        
        let mdf2 = DateFormatter()
        mdf2.dateFormat = "MM"
        let m2 = mdf2.string(from: Date())
        
        let ydf2 = DateFormatter()
        ydf2.dateFormat = "yyyy"
        let y2 = ydf2.string(from: Date())
        
        var diff = Int(y2)! - Int(y1)!
        
        if Int(m2)! - Int(m1)! < 0 {
            diff -= 1
        } else if Int(m2)! == Int(m1)! && Int(d2)! - Int(d1)! < 0{
            diff -= 1
        }
        
        if diff < 0{
            age = -1
        } else {
            age = diff
        }
        
        if age < 17 {
            pushAlert(title: "Oops!", message: "You must be at least 17 years old!")
        }
        
    }
    
    func validateUsername() -> Void {
        if usernameField.text!.count <= 5 || usernameField.text!.count >= 13{
            pushAlert(title: "Check Username!", message: "Username length must be more than 5 and less than 13 characters!")
        }
    }
    
    func validatePassword() -> Void {
        var containsLetter:Bool = false, containsNumber:Bool = false
        for i in passwordField.text!{
            if i.isLetter{
                containsLetter = true
            } else if i.isNumber {
                containsNumber = true
            }
        }
        
        if !containsNumber || !containsLetter {
            pushAlert(title: "Check Password!", message: "Password must contains digit and alphabet!")
        }
        
    }
    
    @IBAction func openAccountBtn(_ sender: Any) {
        validateName()
        validateIdCardNumber()
        validateDOB()
        validateUsername()
        validatePassword()
        performSegue(withIdentifier: "toHomepageSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toHomepageSegue" {
            let destination = segue.destination as! HomepageViewController
            destination.username = usernameField.text!
            destination.name = nameField.text!
            destination.idCardNumber = idCardNumberField.text!
            destination.dob = DOB
        }
    }
    
    @IBAction func unwindToRegister(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
        
        nameField.text = ""
        idCardNumberField.text = ""
        usernameField.text = ""
        passwordField.text = ""
        datepicker.date = Date()
    }
    
    

}

