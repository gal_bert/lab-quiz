//
//  CheckoutViewController.swift
//  DLBK
//
//  Created by Gregorius Albert on 06/11/21.
//

import UIKit

class CheckoutViewController: UIViewController {

    @IBOutlet weak var mo1Label: UILabel!
    @IBOutlet weak var mo2Label: UILabel!
    @IBOutlet weak var mo3Label: UILabel!
    
    var mo1:Double = 0
    var mo2:Double = 0
    var mo3:Double = 0
    
    var checkout:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mo1Label.text = formatNumber(num: String(Int(mo1)))
        mo2Label.text = formatNumber(num: String(Int(mo2)))
        mo3Label.text = formatNumber(num: String(Int(mo3)))
    }
    
    func formatNumber(num:String) -> String{
        var result = ",00"
        var count = 0
        
        for i in num.reversed(){
            count += 1
            result = "\(i)\(result)"
            if count % 3 == 0 && count != num.count{
                result = ".\(result)"
            }
        }
        return result
    }

    
    
    @IBAction func checkoutBtn(_ sender: Any) {
        checkout = true
    }
    
    
    

}
