//
//  ProfileViewController.swift
//  DLBK
//
//  Created by Gregorius Albert on 06/11/21.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var idCardNumberField: UILabel!
    @IBOutlet weak var dobField: UILabel!
    @IBOutlet weak var usernameField: UILabel!
    
    var name:String = ""
    var idCardNumber:String = ""
    var dob:String = ""
    var username = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameField.text = name
        idCardNumberField.text = idCardNumber
        dobField.text = dob
        usernameField.text = username
    }

}
