//
//  HomepageViewController.swift
//  DLBK
//
//  Created by Gregorius Albert on 06/11/21.
//

import UIKit

class HomepageViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var topUpField: UITextField!
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var mo1Field: UITextField!
    @IBOutlet weak var mo2Field: UITextField!
    @IBOutlet weak var mo3Field: UITextField!
    
    var username:String = ""
    var name:String = ""
    var idCardNumber:String = ""
    var dob:String = ""
    
    var balance:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = "Hello, \(name)"
        balanceLabel.text = formatNumber(num: String(balance))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func formatNumber(num:String) -> String{
        var result = ",00"
        var count = 0
        
        for i in num.reversed(){
            count += 1
            result = "\(i)\(result)"
            if count % 3 == 0 && count != num.count{
                result = ".\(result)"
            }
        }
        return result
    }
    
    @IBAction func topUpBtn(_ sender: Any) {
        if(topUpField.text == ""){
            pushAlert(title: "Oops!", message: "Top up must be a value!")
        } else {
            balance += Int(topUpField.text!)!
            balanceLabel.text = formatNumber(num: String(balance))
        }
    }
    
    func pushAlert(title:String, message:String) -> Void{
        
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "OK",
            style: .default,
            handler: nil
        ))
        
        present(alert, animated: true, completion: nil)
    }
    
    var totalDeposit = 0
    @IBAction func makeDepositBtn(_ sender: Any) {
        
        if mo1Field.text == "" || mo2Field.text == "" || mo3Field.text == "" {
            pushAlert(title: "Oops!", message: "Please select a deposit by filling the value!")
        }
        else {
            
            totalDeposit = Int(mo1Field.text!)! + Int(mo2Field.text!)! + Int(mo3Field.text!)!
            if Int(mo1Field.text!)! < 100000 && Int(mo2Field.text!)! < 500000 && Int(mo3Field.text!)! < 1000000{
                pushAlert(title: "Sorry", message: "Your deposit is below the minimum nominal!")
            }
            else if balance < totalDeposit {
                pushAlert(title: "Sorry", message: "Your balance is not enough. Please top up first!")
            }
            else {
                performSegue(withIdentifier: "toCheckoutPageSegue", sender: self)
            }
        }
        
    }
    
    @IBAction func profileBtn(_ sender: Any) {
        performSegue(withIdentifier: "toProfilePageSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toProfilePageSegue"{
            let destination = segue.destination as! ProfileViewController
            destination.name = name
            destination.username = username
            destination.idCardNumber = idCardNumber
            destination.dob = dob
        }
        else if segue.identifier == "toCheckoutPageSegue" {
            let destination = segue.destination as! CheckoutViewController
            destination.mo1 = Double(mo1Field.text!)! + (Double(mo1Field.text!)! * (6 / 100.0) / 12.0) * 1.0
            destination.mo2 = Double(mo2Field.text!)! + (Double(mo2Field.text!)! * (6.5 / 100.0) / 12.0) * 6.0
            destination.mo3 = Double(mo3Field.text!)! + (Double(mo3Field.text!)! * (7 / 100.0) / 12.0) * 12.0
        }
    }
    
    @IBAction func unwindToHomepage(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
       
        if let source = unwindSegue.source as? CheckoutViewController{
            if source.checkout == true{
                let afterDeposit = balance - totalDeposit
                balance = afterDeposit
                balanceLabel.text = formatNumber(num: String(balance))
                source.checkout = false
            }
        }
    }
    

}
